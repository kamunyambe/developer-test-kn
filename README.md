# Databee Full-stack Developer Test

This is my submission of the Databee Developer test.

Prerequisites
Database Engine: mySql PHP Framework: Laravel 5.5

Installation
Run "npm install" to install frontend packages. Create a database and setup connection to it via the .env file. Run "php artisan migrate" to create the database schema. Run "php artisan db:seed" to generate data. You may need to run this several times if to change the data.

PS: The app is located in the "examVenue subfolder"

## Extra Credit

If you have time and are inspired to delve a little further into this expand your platform to include multiple host venues.  Your front-end interface should be updated to allow browsing the availability for multiple venues (either individually or combined).

## Any questions?

Email any questions to phil@databee.com.au.