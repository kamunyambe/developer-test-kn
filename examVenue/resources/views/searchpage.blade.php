<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Kamu Nyambe::Databee Exam Venues</title>

    <!-- Fonts -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="//use.fontawesome.com/a7cb3cabb8.js"></script>

</head>
<body>
    <div class="container mt-4">
        <div class="col-sm-12 mt-5">
            <form id="search_form">
                <div class="form-group">
                    <label for="datepicker">Exam Date:</label>
                    <input type="text" class="form-control" name="date" placeholder="Please Select a Date" aria-label="Date Selection" id="datepicker" />
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="button" id="reset" class="btn btn-sm btn-primary pull-right ml-2" value="Clear Search" />
                        <button class="btn btn-sm btn-success pull-right ml-2">Search</button>
                    </div>
                </div>
            </form>
            <div class="row">
                <div id="results" class="col-sm-12">

                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script>
        //placeholder and progress markup for results table
        var empty = '<p class="mt-5">There are no results to show</p>';

        var progress = '<div class="progress col-sm-12 mt-5">\n' +
            '  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Loading...</div>\n' +
            '</div>';

        //init our datepicker
        $('#datepicker').datepicker({
            startDate: '0d',
            endDate: '+3m',
            autoclose:true,
            format: 'dd-mm-yyyy'
        });

        //searching
        $('#search_form').submit(function(e){
            e.preventDefault();
            showHideProgress(progress);
            var form = $(this);
            var data=form.serialize();

            $.get('/api/search_venues/'+data,function (data, textStatus, jqXhr) {
                let items='';
                if(jqXhr.status==200){
                    items=showResults(data);
                }else{
                    items=empty;
                }
                showHideProgress(items);
            })
            .fail(function(){
                showHideProgress(progress);
            })
        });

        //a small fn to toggle progress indicator
        function showHideProgress(dom){
            $('#results').empty();
            $('#results').append(dom);
        }

        //Clearing the search
        $('#reset').on('click', function(e){
            e.stopImmediatePropagation();
            $('#search_form').trigger("reset");
            $('#datepicker').val("");
            showHideProgress(empty);
        })

        //$results filler helper fn
        function showResults(data){
            var results = '<div class="row">';
            $.each(data, function(idx, val){
                var card = "<div class='col-sm-4 mt-4'>";
                card +="<div class='card'>";
                card +="<div class='card-header'>"+idx+"</div>";
                card +="<div class='card-body'>";
                $.each(val, function(time, details){
                    card += (details.status==='available') ? "<div class='col-xs-12 bg-success>'" : "<div class='col-xs-12 bg-danger>'";
                    card += "<p><strong>"+details.name+"</strong></p>";
                    card += "<p><strong>Exam Time</strong>: <i>"+details.slot+"</i></p>";
                    card += (details.status==='available') ? "<h6> <span class='badge badge-success'>Available</span></h6>" : "<h6> <span class='badge badge-danger'>Booked</span></h6>"
                    card += "</div>";
                    card += "<hr />";
                });
                card += "</div>";
                card += "</div>";
                card += "</div>";
                results +=card;
            });
            results += "</div>";
            return results;
        }

    </script>
</body>
</html>
