<?php

use Illuminate\Database\Seeder;
use App\exam_bookings;

class examBookingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Here I make sure that the exam bookings table is empty
        exam_bookings::query()->truncate();

        //the 600 below is somewhat a magic number, no special meaning to it but just an estimate of bookings over 4 months
        $bookings = factory(App\exam_bookings::class, 600)->make();
        foreach ($bookings as $booking){
            repeat:
            try{
                $booking->save();
            }catch (\Illuminate\Database\QueryException $e){//Here i'm handling a unique key constraint issue
                $booking = factory(App\exam_bookings::class)->make();
                goto repeat;
            }
        }
    }
}
