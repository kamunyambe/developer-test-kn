<?php

use Illuminate\Database\Seeder;
use App\exam_times;

class examTimesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Here I make sure that the exam times table is empty
        exam_times::query()->truncate();

        //Fake exam times array ( key=name, value=time_slot )
        $times=['Morning'=>'08:00am to 11:30am', 'Afternoon'=>'13:00pm to 16:30pm', 'Evening'=>'18:00pm to 20:30pm'];

        foreach ($times as $name=>$slot){
            exam_times::query()->create([
                'name'=>$name,
                'time_slot'=>$slot
            ]);
        }

    }
}
