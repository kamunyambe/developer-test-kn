<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(App\exam_bookings::class, function (Faker $faker) {
    //First we get a list of id's from the exam_times model
    $times = DB::table('exam_times')->pluck('id')->toArray();
    return [
        'exam_times_id'=>$faker->randomElement($times),
        'exam_date'=>$faker->dateTimeBetween('-1 days', '+4 months')
    ];
});
