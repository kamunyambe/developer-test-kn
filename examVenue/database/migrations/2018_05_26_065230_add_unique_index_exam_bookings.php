<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIndexExamBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_bookings', function (Blueprint $table) {
            $table->unique(['exam_times_id', 'exam_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_bookings', function (Blueprint $table) {
            $table->dropUnique(['exam_times_id', 'exam_date']);
        });
    }
}
