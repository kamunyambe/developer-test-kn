<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchVenues extends Controller
{
    public function check($date){
        $bookings=new \App\exam_bookings();
        $filter = [];
        parse_str($date, $filter);
        $date=$filter['date'];
        try{
            $result = $bookings->searchBookings($date);
        }catch(\Exception $exception){
            $error = $exception->getMessage();
            return response()->json(['error'=>$error], '204');
        }

        return response()->json($result , '200');
    }
}
