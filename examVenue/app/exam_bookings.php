<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\exam_times;
use Carbon\Carbon;

class exam_bookings extends Model
{
    //Build Search Query
    function searchBookings($date){
        $date = Carbon::parse($date);
        $exam_times = exam_times::all();//this gets all possible exam times
        $date_results = [];//this will hold our reults
        $target=$date->toDateString();
        $prev=$date->subDay()->toDateString();
        $post=$date->addDay(2)->toDateString();
        $search_dates = [$prev, $target, $post]; //Day before, search date, day after
        foreach ($exam_times as $time){
            foreach ($search_dates as $day){
                $booked = exam_bookings::where('exam_times_id', '=', $time->id)
                    ->where('exam_date', '=', $day)->exists();

                $key=Carbon::parse($day)->toFormattedDateString();
                if($booked){
                    $date_results[$key][$time->name]=['name'=>$time->name, 'slot'=>$time->time_slot, 'status'=>'booked'];
                }else{
                    $date_results[$key][$time->name]=['name'=>$time->name, 'slot'=>$time->time_slot, 'status'=>'available'];
                }
            }
        }
        return $date_results;
    }
}
